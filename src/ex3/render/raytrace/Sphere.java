package ex3.render.raytrace;

import java.util.Map;

import math.Point3D;
import math.Ray;
import math.Vec;

public class Sphere extends Surface {

	private Point3D center;
	private double radius;
	
	/**
	 * C'tor
	 * 
	 * Initializes a new Sphere
	 */
	public Sphere() {
		super();
	}
	
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		super.init(attributes);
		
		this.center = new Point3D(attributes.get("center"));
		this.radius = Double.parseDouble(attributes.get("radius"));
	}

	@Override
	public Intersection intersects(Ray r, boolean facesUp) {

		// Find the 2 intersection points of the ray with this Sphere
		// denoted as t1, t2
		double a = 1;
		double b = Vec.dotProd(Vec.scale(2, r.getDirection()), Vec.fromToPoints(center, r.getOrigin()));
		double c = Vec.fromToPoints(center, r.getOrigin()).lengthSquared() - radius * radius;
		
		double delta = b * b - 4 * a * c;
		
		if (delta < 0) {
			return null;
		}
		
		double t1 = (0 - b + Math.sqrt(delta)) / (2 * a);
		double t2 = (0 - b - Math.sqrt(delta)) / (2 * a);
		
		// Return the nearest point
		double scalar = Math.min(t1, t2);
		
		if (!facesUp) {
			// Return the farthest point
			scalar = Math.max(t1, t2);
		}
		
		// Ray MUST NOT intersect in the negative direction 
		if (scalar < 0) {
			return null;
		}
		
		Point3D ret = r.getOrigin().clone();
		ret.add(Vec.scale(scalar, r.getDirection()));
		
		if (facesUp && r.getDirection().dotProd(getNormal(ret)) > 0) {
			return null;
		}
		
		if (!facesUp && r.getDirection().dotProd(getNormal(ret)) < 0) {
			return null;
		}
		
		return new Intersection(ret, this, r);
	}
	
	@Override
	public Vec getNormal(Point3D p) {
		Vec normal = Vec.fromToPoints(center, p);
		normal.normalize();
		
		return normal;
	}
	
	/**
	 * Check whether point 'p' is inside this Sphere.
	 * 
	 * @param p 	Some point
	 * @return		True if 'p' inside this Sphere,
	 * 				false otherwise.
	 */
	public boolean isPointInside(Point3D p) {
		return center.distanceFrom(p) <= radius;
	}
}

