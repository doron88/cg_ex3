package ex3.render.raytrace;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import ex3.parser.Element;
import ex3.parser.SceneDescriptor;
import ex3.render.IRenderer;

public class RayTracer implements IRenderer {

	// Used to round up precision problems
	public static double EPSILON = 0.0001;
	private Scene scene;
	
	/**
	 * Inits the renderer with scene description and sets the target canvas to
	 * size (width X height). After init renderLine may be called
	 * 
	 * @param sceneDesc
	 *            Description data structure of the scene
	 * @param width
	 *            Width of the canvas
	 * @param height
	 *            Height of the canvas
	 * @param path
	 *            File path to the location of the scene. Should be used as a
	 *            basis to load external resources (e.g. background image)
	 */
	@Override
	public void init(SceneDescriptor sceneDesc, int width, int height, File path) {

		scene = new Scene(width, height);
		scene.init(sceneDesc.getSceneAttributes());
		
		for (Element e : sceneDesc.getObjects()) {
			try {
				scene.addObjectByName(e.getName(), e.getAttributes());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		try {
			scene.setCameraAttributes(sceneDesc.getCameraAttributes());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Renders the given line to the given canvas. Canvas is of the exact size
	 * given to init. This method must be called only after init.
	 * 
	 * @param canvas
	 *            BufferedImage containing the partial image
	 * @param line
	 *            The line of the image that should be rendered.
	 */
	@Override
	public void renderLine(BufferedImage canvas, int line) {
		int height = canvas.getHeight();
		int width = canvas.getWidth();
		
		int y = line;
		
		for (int x = 0 ; x < canvas.getWidth() ; ++x) {
			Color color = scene.getColor(x, y, height, width);
			canvas.setRGB(x, y, color.getRGB());
		}
	}
}
