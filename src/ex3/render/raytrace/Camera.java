package ex3.render.raytrace;

import java.util.Map;
import java.util.Scanner;

import math.Point3D;
import math.Ray;
import math.Vec;


/**
 * Represents the scene's camera.
 */
public class Camera implements IInitable {

	private Point3D eye;
	private Vec toDirection;
	private Vec upDirection;
	private Vec rightDirection;
	private double distance;
	private double width;
	
	/***
	 * C'tor.
	 * Initiates default values.
	 */
	public Camera() {
		eye = null;
		toDirection = null;
		upDirection = null;
		rightDirection = null;
		width = 2.0;
	}
	
	/***
	 * Loads properties from given attributes.
	 * @param attributes Attributes to read. 
	 * @throws Exception 
	 */
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		Scanner scanner = null;
		
		for (Map.Entry<String, String> item : attributes.entrySet()) {
			String key = item.getKey();
			String val = item.getValue();

			scanner = new Scanner(val);
			
			if (key.equalsIgnoreCase("eye")) {
				eye = new Point3D(val);
			} else if (key.equalsIgnoreCase("direction")) {
				toDirection = new Vec(val);
			} else if (key.equalsIgnoreCase("look-at")) {
				toDirection = eye.directionTo(new Point3D(val));
			} else if (key.equalsIgnoreCase("up-direction")) {
				upDirection = new Vec(val);
			} else if (key.equalsIgnoreCase("screen-dist")) {
				distance = scanner.nextDouble();
			} else if (key.equalsIgnoreCase("screen-width")) {
				width = scanner.nextDouble();
			}
		}

		if (null == upDirection) {
			throw new Exception("camera's up-direction not given");
		}
		if (null == toDirection) {
			throw new Exception("camera's direction not given");
		}

		toDirection.normalize();
		upDirection.normalize();
		
		rightDirection = Vec.crossProd(toDirection, upDirection);
		rightDirection.normalize();
		
		upDirection = Vec.crossProd(rightDirection, toDirection);
		upDirection.normalize();
	}
	
	/**
	 * Transforms image xy coordinates to view pane xyz coordinates. Returns the
	 * ray that goes through it.
	 * 
	 * @param x X coordinate in view-pane.
	 * @param y Y coordinate in view-pane.
	 * @return Returns the ray the passes through the given x and y coordinates.
	 */
	public Ray constructRayThroughPixel(double x, double y, double height, double width) {
		
		// Start from image center
		Point3D imageCenter = eye.clone();
		imageCenter.add(Vec.scale(distance, toDirection));
		
		// Calculate ratio between view-pane and camera width
		double ratio = this.width / width;
		
		double rightScalar = (x - width / 2) * ratio;
		double upScalar = (y - height / 2) * ratio; 
		
		Vec rightVec = Vec.scale(rightScalar, rightDirection);
		Vec upVec = Vec.scale(upScalar, upDirection); 
		
		// Get the point in view-pane 
		Point3D p = new Point3D(imageCenter);
		p.add(rightVec);
		p.sub(upVec);
		
		// Construct the direction vector between the eye and the point
		// hit in the view-pane.
		Vec direction = Vec.fromToPoints(eye, p);
		
		return new Ray(eye, direction);
	}

	/***
	 * Returns the distance of the given point from the eye.
	 * @param p Point to calculate the distance from.
	 * @return The desired distance. 
	 */
	public double distanceFrom(Point3D p) {
		return eye.distanceFrom(p);
	}
}
