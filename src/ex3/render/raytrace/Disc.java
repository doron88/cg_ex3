package ex3.render.raytrace;
import java.util.Map;

import math.Point3D;
import math.Ray;
import math.Vec;

public class Disc extends Surface {

	private Point3D center;
	private double radius;
	private Vec normal;
	
	public Disc() {
		super();
	}
	
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		super.init(attributes);
		
		center = new Point3D(attributes.get("center"));
		radius = Double.parseDouble(attributes.get("radius"));
		normal = new Vec(attributes.get("normal"));
		
		if (null == center) {
			throw new Exception("Disc must contain and center value");
		}
		
		if (0 >= radius) {
			throw new Exception("Disc's radius cannot be <= 0");
		}
		
		if (null == normal) {
			throw new Exception("Disc must contain a normal");
		}
		
		normal.normalize();
	}

	@Override
	public Intersection intersects(Ray r, boolean facesUp) {
		if ((facesUp && r.getDirection().dotProd(normal) > 0) || 
				(!facesUp && r.getDirection().dotProd(normal) < 0)) {
			return null;
		}

		// First we get the intersection with the disc's plane
		Point3D intersectionPoint = r.getIntersectionPoint(center, normal);
		
		// Now verify if it's in the disc's radius
		if (intersectionPoint.distanceFrom(center) > radius) {
			return null;
		}
		
		return new Intersection(intersectionPoint, this, r);
	}
	
	@Override
	public Vec getNormal(Point3D p) {
		return normal;
	}
}
