package ex3.render.raytrace;

import java.util.Map;
import java.util.Scanner;

import math.Point3D;
import math.Ray;
import math.Vec;

public class SpotLight extends Light {

	private Point3D pos;
	private Vec direction;
	
	/**
	 * Light attenuation coefficients
	 */
	private double kc;
	private double kl;
	private double kq;

	/**
	 * C'tor
	 * Initialize new spotlight.
	 * defaults coefficients: kc=1, kl=0, kq=0
	 */
	public SpotLight() {
		super();
		
		kc = 1;
		kl = 0;
		kq = 0;
	}
	
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		super.init(attributes);
		
		for (Map.Entry<String, String> item : attributes.entrySet()) {
			String key = item.getKey();
			String val = item.getValue();

			Scanner scanner = new Scanner(val);
			
			if (key.equalsIgnoreCase("pos")) {
				pos = new Point3D(val);
			} else if (key.equalsIgnoreCase("kc")) {
				kc = scanner.nextDouble();
			} else if (key.equalsIgnoreCase("kl")) {
				kl = scanner.nextDouble();
			} else if (key.equalsIgnoreCase("kq")) {
				kq = scanner.nextDouble();
			} else if (key.equalsIgnoreCase("dir")) {
				direction = new Vec(val);
				direction.normalize();
			}
			
			scanner.close();
		}
	}


	@Override
	public Vec getDirection(Point3D p) {
		Vec direction = Vec.fromToPoints(p, pos);
		direction.normalize();
		return direction;
	}

	@Override
	public Vec getColor(Point3D p) {
		double distance = pos.distanceFrom(p);
		Vec lightVector = Vec.fromToPoints(pos, p);
		lightVector.normalize();
		
		double dl = direction.dotProd(lightVector);
		double attenuation = dl / (kc + kl * distance + kq * distance * distance);
		
		Vec intensity = this.color.clone();
		intensity.scale(attenuation);
		
		return intensity;
	}
	
	@Override
	public boolean isShadowed(Point3D p, Surface surface) {
		Ray ray = new Ray(pos, Vec.fromToPoints(pos, p));
		Intersection intersection = surface.intersects(ray, true);
		
		if (intersection != null && intersection.getPoint().distanceFrom(p) > RayTracer.EPSILON) {
			return true;
		}
		return false;
		
	}

	@Override
	public boolean isFirstCloser(Point3D p1, Point3D p2) {
		return pos.distanceFrom(p1) < pos.distanceFrom(p2);
	}
}
