package ex3.render.raytrace;

import java.util.Map;
import java.util.Scanner;

import math.Point3D;
import math.Ray;
import math.Vec;

public class DirectionalLight extends Light {

	private Vec direction;
	
	public DirectionalLight() {
		super();
	}
	
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		
		super.init(attributes);
		
		for (Map.Entry<String, String> item : attributes.entrySet()) {
			String key = item.getKey();
			String val = item.getValue();

			Scanner scanner = new Scanner(val);
			
			if (key.equalsIgnoreCase("direction")) {
				direction = new Vec(val);
				direction.normalize();
			}
			
			scanner.close();
		}
	}
	
	@Override
	public Vec getDirection(Point3D p) {
		return Vec.negate(direction);
	}
	
	@Override
	public Vec getColor(Point3D p) {
		return color;
	}
	
	@Override
	public boolean isShadowed(Point3D p, Surface surface) {
		Ray ray = new Ray(p, direction);
		Intersection intersection = surface.intersects(ray, true);
		
		if (intersection != null && intersection.getPoint().distanceFrom(p) > RayTracer.EPSILON) {
			return true;
		}
		return false;	
	}
	
	@Override
	public boolean isFirstCloser(Point3D p1, Point3D p2) {
		return true;
	}
	
}



