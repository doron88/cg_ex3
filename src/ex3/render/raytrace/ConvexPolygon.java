package ex3.render.raytrace;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import math.Point3D;
import math.Ray;
import math.Vec;

public class ConvexPolygon extends Surface {

	private List<Point3D> points;
	private Vec normal;
	
	public ConvexPolygon() {
		super();
	}
	
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		super.init(attributes);
		
		points = new ArrayList<Point3D>();
		
		for (Map.Entry<String, String> item : attributes.entrySet()) {
			String key = item.getKey();
			String val = item.getValue();

			Scanner scanner = new Scanner(val);
			
			if (key.toLowerCase().startsWith("p")) {
				points.add(new Point3D(val));
			}
			
			scanner.close();
		}
		
		if (points.size() < 3) {
			throw new Exception("ConvexPolygon must contain at least 3 points");
		}
		
		normal = Vec.crossProd(Vec.fromToPoints(points.get(0), points.get(1)),
				Vec.fromToPoints(points.get(0), points.get(points.size() - 1)));
		normal.normalize();
	}
	
	@Override
	public Intersection intersects(Ray r, boolean facesUp) {

		if ((facesUp && (r.getDirection().dotProd(normal) >  RayTracer.EPSILON)) || 
				(!facesUp && (r.getDirection().dotProd(normal) <  RayTracer.EPSILON))) {
			return null;
		}

		for (int i = 1 ; i < points.size() - 1 ; ++i) {
			
			// Each 3 points build up a triangle
			Triangle tri = new Triangle(points.get(0), points.get(i), points.get(i + 1));
			
			Intersection intersection = tri.intersects(r, true);
			
			if (null != intersection) {

				// Return a new intersection so that the surface object will be 
				// the convex polygon instead of the triangle
				return new Intersection(intersection.getPoint(), this, r);
			}
		}
		
		// Reaching here means not triangle within the polygon has an intersection
		// with the given ray
		return null;
	}

	@Override
	public Vec getNormal(Point3D p) {
		return normal;
	}
}
