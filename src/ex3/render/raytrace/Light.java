package ex3.render.raytrace;
import java.util.Map;

import math.Point3D;
import math.Vec;

/**
 * Represent a point light
 * 
 * Add methods as you wish, this can be a super class for other lights (think which)
 */
public abstract class Light implements IInitable {
	
	protected Vec color;
	
	/**
	 * C'tor
	 * defaults to white light color
	 */
	public Light() {
		color = new Vec(1, 1, 1);
	}

	@Override
	public void init(Map<String, String> attributes) throws Exception {
		if (attributes == null) {
			throw new Exception("Light attributes is null");
		}
		if (attributes.containsKey("color")){
			color = new Vec(attributes.get("color"));
		}
	}
	
	/**
	 * Get a direction vector from point 'p' to this light
	 * @param p 	Starting point 
	 * @return		Direction vector 
	 */
	public abstract Vec getDirection(Point3D p);
	
	/**
	 * Get the light attenuation at a given point 'p'
	 * 
	 * @param p		A point
	 * @return		The color added by this light 
	 */
	public abstract Vec getColor(Point3D p);
	
	/**
	 * Checks if a surface is shadowed by this light at a given point 'p'. 
	 * 
	 * @param p			Intersection point on the surface
	 * @param surface	A surface
	 * @return			True, if 'surface' is shadowed by this light in point 'p',
	 * 					false otherwise. 
	 */
	public abstract boolean isShadowed(Point3D p, Surface surface);
	
	/**
	 * Check if point 'p1' is closer than 'p2' to this light.
	 * 
	 * @param p1	Point
	 * @param p2	Point
	 * @return		True, if p1 is closer from p2 to this list,
	 * 				false otherwise. 
	 */
	public abstract boolean isFirstCloser(Point3D p1, Point3D p2);
}
