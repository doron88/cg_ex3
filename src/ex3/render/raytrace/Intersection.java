package ex3.render.raytrace;
/**
 * Intersection class
 * 
 * Stores an information about intersection points in the scene. 
 */
import math.Point3D;
import math.Ray;
import math.Vec;

public class Intersection {

	/**
	 * Intersection point
	 */
	private Point3D point;
	/**
	 * Intersected surface
	 */
	private Surface surface;
	/**
	 * The ray which intersects surface
	 */
	private Ray ray;
	/**
	 * Normal at intersection point
	 */
	private Vec normal;
	
	
	/**
	 * Initializes new Intersection object
	 * 
	 * @param p 	Intersection point
	 * @param s		Intersected surface
	 * @param r		The ray which intersects surface
	 */
	public Intersection(Point3D p, Surface s, Ray r) {
		point = p;
		surface = s;
		ray = r;
		normal = s.getNormal(p);
	}
	
	/**
	 * Get the ray which intersects the surface
	 * @return	Ray
	 */
	public Ray getRay() {
		return ray;
	}
	
	/**
	 * Get the intersection point
	 * @return	Point of intersection
	 */
	public Point3D getPoint() {
		return point;
	}
	
	/**
	 * Get the intersected surface
	 * @return surface
	 */
	public Surface getSurface() {
		return surface;
	}
	
	/**
	 * Get the normal at intersection point
	 * @return	normal
	 */
	public Vec getNormal() {
		return normal;
	}
}
