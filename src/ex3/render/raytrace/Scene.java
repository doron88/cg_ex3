package ex3.render.raytrace;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.imageio.ImageIO;

import math.Ray;
import math.Vec;

/**
 * A Scene class containing all the scene objects including camera, lights and
 * surfaces.
 */
public class Scene implements IInitable {

	private List<Surface> surfaces;
	private List<Light> lights;
	private Camera camera;

	private Vec backgroundColor;
	private Vec ambientLight;
	private BufferedImage backgroundTexture;
	private int maxRecursionLevel;
	
	// Dimensions for output view-pane
	private int width;
	private int height;
	
	/***
	 * C'tor
	 * @param width		Width of output view-pane
	 * @param height	Height of output view-pane
	 */
	public Scene(int width, int height) {

		// Init everything to default values
		backgroundColor = new Vec(0, 0, 0);
		ambientLight = new Vec(0, 0 , 0);
		backgroundTexture = null;
		maxRecursionLevel = 10;
		surfaces = new LinkedList<Surface>();
		lights = new LinkedList<Light>();
		camera = new Camera();
		this.width = width;
		this.height = height;
	}

	/***
	 * Loads scene properties
	 * @param attributes	Map of all scene properties
	 */
	public void init(Map<String, String> attributes) {
		Scanner scanner = null;
		
		for (Map.Entry<String, String> item : attributes.entrySet()) {
			String key = item.getKey();
			String val = item.getValue();

			scanner = new Scanner(val);

			if (key.equalsIgnoreCase("background-col")) {
				
				float r = scanner.nextFloat();
				float g = scanner.nextFloat();
				float b = scanner.nextFloat();
				
				backgroundColor = new Vec(r, g, b);
				
			} else if (key.equalsIgnoreCase("ambient-light")) {
				
				float r = scanner.nextFloat();
				float g = scanner.nextFloat();
				float b = scanner.nextFloat();
				
				ambientLight = new Vec(r, g, b);
			} else if (key.equalsIgnoreCase("background-tex")) {
				
				backgroundTexture = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
				File file = new File(val);
				
				try {
					backgroundTexture = ImageIO.read(file);
				} catch (IOException e) {
					System.out.printf("Given background texture images couldn't be found.\nPlease specify full path\n");
				}
				
			} else if (key.equalsIgnoreCase("max-recursion-level")) {
				
				maxRecursionLevel = Integer.parseInt(val);
			} 
		}
	}

	/***
	 * Gets the color in the given x and y coordinates
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @return Color in that position
	 */
	public Vec getBackgroundColor(int x, int y) {
		if (null == backgroundTexture) {
			return backgroundColor;
		}
		
		int xRatio = backgroundTexture.getWidth() / width;
		int yRatio = backgroundTexture.getHeight() / height;
		
		return new Vec(new Color(backgroundTexture.getRGB(x * xRatio, y * yRatio)));
	}

	/**
	 * Searches for the nearest intersection with the given ray.
	 * 
	 * @param ray 		Ray that is shot.
	 * @param faceUp	Should the intersection look for surfaces that face up.
	 * @return 			Nearest intersection. Null if none is found.
	 */
	public Intersection findIntersection(Ray ray, boolean facesUp) {
		Intersection minIntersection = null;
		double minDistance = 0;
		
		for (Surface s : surfaces) {
			Intersection intersection = s.intersects(ray, facesUp);
			
			if (null != intersection) {
				double currentDistance = ray.getOrigin().distanceFrom(intersection.getPoint());

				if ((null == minIntersection) || (currentDistance < minDistance)) {
					// Update nearest intersection
					minIntersection = intersection;
					minDistance = currentDistance;
				}
			}
		}
		
		return minIntersection;
	}

	/***
	 * Calculates the color for the given ray and intersection.
	 * This recursive method will also follow reflections according to
	 * the defined maxRecursionLevel and reflectance of different surfaces
	 * @param x 			X coordinate in the view pane in case of no intersection.
	 * @param y   			Y coordinate in the view pane in case of no intersection.
	 * @param intersection 	Intersection to calculate at.
	 * @param ray 			Hitting ray.
	 * @param level 		Recursion level. Starts from maxRecursionLevel and decrements down to 0.
	 * @return Color in given intersection.
	 */
	public Vec calcColor(int x, int y, Intersection intersection, Ray ray, int level) {
		
		if (level == 0) {
			// No color to add. Stop recursion here.
			return new Vec(0, 0, 0);
		}
		
		if (null == intersection) {
			// Not intersection means just the background
			return getBackgroundColor(x, y).clone();
		}

		// Start summing up the RGB intensity in the given intersection 
		Vec intensity = new Vec();
		
		// Normal in the hitted surface
		Vec normal = intersection.getNormal();
		
		// Ambient and Emission calculations
		Surface surface = intersection.getSurface();
		intensity.add(surface.getMtlEmission());
		intensity.add(Vec.scale(surface.getMtlAmbient(), ambientLight));
		
		// Diffuse & Specular calculations
		for (Light light : lights) {

			// Look for other surfaces shadowing our main one
			
			boolean shadowedByAnother = false;
			
			// Notice that directional light has no position and therefore cannot
			// cast a shadow
			
			if (!(light instanceof DirectionalLight)) {
				for (Surface anotherSurface : surfaces) {
					
					if (surface == anotherSurface) {
						// Same surface. This check is done in a different flow.
						continue;
					}
					
					// Look for any other surface blocking the light source
					
					Vec direction = light.getDirection(intersection.getPoint());
					Intersection tmpIntersection = anotherSurface.intersects(new Ray(intersection.getPoint(), direction), false);
	
					if (tmpIntersection != null) {
						if (light.isFirstCloser(tmpIntersection.getPoint(), intersection.getPoint())) {
							// Other surface is also closer to light source and faces it up.
							// Therefore, it blocks the light source.
							
							shadowedByAnother = true;
							break;
						}
					}
				}
			}
			
			if (shadowedByAnother) {
				// Shadowed by another surface. Skip this light source ray cast.
				continue;
			}
			
			if (light.isShadowed(intersection.getPoint(), intersection.getSurface())) {
				// Shadowed by self. Skip this light source ray cast.
				continue;
			}
			
			// Calculation of diffuse light
			
			Vec lightColor = light.getColor(intersection.getPoint());
			Vec lightVector = light.getDirection(intersection.getPoint());
			double nDotL = normal.dotProd(lightVector);
			Vec diffuse = Vec.scale(nDotL, surface.getMtlDiffuse());
			diffuse.scale(lightColor);
			
			intensity.add(diffuse);

			// Calculation of specular light
			
			Vec eyeVec = intersection.getRay().getDirection();
			eyeVec.normalize();
			Vec lightRayReflected = lightVector.clone().reflect(intersection.getNormal());
			double vDotR = eyeVec.dotProd(lightRayReflected);
			Vec specular = Vec.scale(Math.pow(vDotR, surface.getMtlShininess()), surface.getMtlSpecular());
			specular.scale(lightColor);
			
			if (vDotR > RayTracer.EPSILON) {
				intensity.add(specular);
			}
		}

		if (surface.getReflectance() > 0) {
			// RaY-tRaCiNg! Start following the rays...
			
			// Get the reflected ray
			Ray reflectedRay = ray.getReflected(intersection.getPoint(), normal);
			
			// Look of its intersection in another surface
			intersection = findIntersection(reflectedRay, true);

			// Get the color in that point
			Vec reflectedIntensity = calcColor(x, y, intersection, reflectedRay, level - 1);
			
			// Scale it accordance to current surface reflectance
			reflectedIntensity.scale(surface.getReflectance());
			
			// Add to global surface intensity at that point
			intensity.add(reflectedIntensity);
		}

		return intensity;
	}
	
	/***
	 * Gets the color of that should appear on the viewer's view-pane in the
	 * given coordinates. 
	 * @param x 		X coordinate in view-pane.
	 * @param y 		Y coordinate in view-pane.
	 * @param height 	View-pane height.
	 * @param width 	View-pane width.
	 * @return Color at the given coordinates.
	 */
	public Color getColor(int x, int y, int height, int width) {
		
		// Construct a ray from eye to nearest intersection
		Ray ray = camera.constructRayThroughPixel(x, y, height, width);
		Intersection eyeIntersection = findIntersection(ray, true);
		
		// Return the color in the intersection point in accordance to the 
		// ray-tracing algorithm
		return calcColor(x, y, eyeIntersection, ray, maxRecursionLevel).toColor();
	}

	/**
	 * Add objects to the scene by name
	 * 
	 * @param name Object's name
	 * @param attributes Object's attributes
	 * @throws Exception 
	 */
	public void addObjectByName(String name, Map<String, String> attributes) throws Exception {
		Surface surface = null;
		Light light = null;
	
		if ("sphere".equalsIgnoreCase(name)) {
			surface = new Sphere();
		}
		
		if ("disc".equalsIgnoreCase(name)) {
			surface = new Disc();
		}
		
		if ("triangle".equalsIgnoreCase(name)) {
			surface = new Triangle();
		}
		
		if ("trimesh".equalsIgnoreCase(name)) {
			// Just a bunch of triangles
			
			for (Map.Entry<String, String> item : attributes.entrySet()) {
				String key = item.getKey();
				String val = item.getValue();

				Scanner scanner = new Scanner(val);
				
				if (key.toLowerCase().startsWith("tri")) {
					Triangle tri = new Triangle(val);
					tri.initOnlyMaterialRelated(attributes);
					surfaces.add(tri);
				}
				
				scanner.close();
			}
		}

			
		if ("convexpolygon".equals(name)) {
			surface = new ConvexPolygon();
		}
		
		if ("omni-light".equals(name)) {
			light = new OmniLight();
		}

		if ("spot-light".equals(name)) {
			light = new SpotLight();
		}

		if ("dir-light".equals(name)) {
			light = new DirectionalLight();
		}
		
		
		// Adds a surface to the list of surfaces
		if (surface != null) {
			surface.init(attributes);
			surfaces.add(surface);
		}

		// Adds a light to the list of lights
		if (light != null) {
			light.init(attributes);
			lights.add(light);
		}
	}

	/***
	 * Updates the camera attributes
	 * @param attributes Given attributes
	 * @throws Exception 
	 */
	public void setCameraAttributes(Map<String, String> attributes) throws Exception {
		this.camera.init(attributes);
	}
}
