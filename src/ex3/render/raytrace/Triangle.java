package ex3.render.raytrace;
import java.util.Map;
import java.util.Scanner;

import math.Point3D;
import math.Ray;
import math.Vec;

public class Triangle extends Surface {

	private Point3D p0;
	private Point3D p1;
	private Point3D p2;
	private Vec normal;
	private Vec v1;
	private Vec v2;
	
	public Triangle() {
		super();
	}
	
	public Triangle(Point3D p0, Point3D p1, Point3D p2) {
		super();
		init(p0, p1, p2);
	}
	
	public Triangle(Scanner scanner) {
		super();
		
		Point3D p0 = new Point3D(scanner);
		Point3D p1 = new Point3D(scanner);
		Point3D p2 = new Point3D(scanner);
		
		init(p0, p1, p2);
	}
	
	public Triangle(String points) {
		super();
		
		Scanner scanner = new Scanner(points);
		Point3D p0 = new Point3D(scanner);
		Point3D p1 = new Point3D(scanner);
		Point3D p2 = new Point3D(scanner);
		scanner.close();
		
		init(p0, p1, p2);
	}
	
	public void init(Point3D p0, Point3D p1, Point3D p2) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
		
		// Normal calculation
		v1 = Vec.fromToPoints(p0, p1);
		v2 = Vec.fromToPoints(p0, p2);
		this.normal = Vec.crossProd(v1, v2);
		this.normal.normalize();
	}
	
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		super.init(attributes);
		
		Point3D p0 = new Point3D(attributes.get("p0"));
		Point3D p1 = new Point3D(attributes.get("p1"));
		Point3D p2 = new Point3D(attributes.get("p2"));
		
		if (p0 == null || p1 == null || p2 == null) {
			throw new Exception("Triangle must contain a definition for p0, p1, and p2.");
		}
		
		init(p0, p1, p2);
	}
	
	@Override
	public Intersection intersects(Ray r, boolean facesUp) {
		if ((facesUp && r.getDirection().dotProd(normal) > 0) || 
				(!facesUp && r.getDirection().dotProd(normal) < 0)) {
			return null;
		}
		
		// First look for the intersection with the triangle's plane
		
		Point3D hit = r.getIntersectionPoint(p0, normal);
		
		if (null == hit) {
			return null;
		}
		
		// Now verify it's within the triangle

		double v1DotV1 = Vec.dotProd(v1, v1); 
		double v1DotV2 = Vec.dotProd(v1, v2); 
		double v2DotV2 = Vec.dotProd(v2, v2); 
		
		Vec w = Vec.fromToPoints(p0, hit);
		double wDotV1 = Vec.dotProd(w, v1); 
		double wDotV2 = Vec.dotProd(w, v2); 
		double D = v1DotV2 * v1DotV2 - v1DotV1 * v2DotV2;
		
		double u1 = (v1DotV2  * wDotV2 - v2DotV2 * wDotV1) / D;
		double u2 = (v1DotV2  * wDotV1 - v1DotV1 * wDotV2) / D;

		// In order for the point to be inside the triangle
		// the sum of the two scalars must be in the range of 0-1.
		// The epsilon constant is use here since double calculations
		// may contains some precision errors.
		
		if (u1 < -RayTracer.EPSILON || u1 > 1 + RayTracer.EPSILON) {
			return null;
		}
		
		if (u2 < -RayTracer.EPSILON || (u1 + u2) > 1 + RayTracer.EPSILON) {
			return null;
		}

		return new Intersection(hit, this, r);
	}
	
	@Override
	public Vec getNormal(Point3D p) {
		return normal;
	}
}
