package ex3.render.raytrace;

import java.util.Map;
import java.util.Scanner;
import math.Point3D;
import math.Ray;
import math.Vec;

public abstract class Surface implements IInitable {
	
	private Vec mtlDiffuse;
	private Vec mtlSpecular;
	private Vec mtlAmbient;
	private Vec mtlEmission;
	private double mtlShininess;
	private double reflectance;
	
	/**
	 * C'tor
	 * Initializes a new surface
	 */
	public Surface() {
		mtlDiffuse = new Vec(0.7, 0.7, 0.7);
		mtlSpecular = new Vec(1, 1, 1);
		mtlAmbient = new Vec(0.1, 0.1, 0.1);
		mtlEmission = new Vec(0, 0, 0);
		mtlShininess = 100;
		reflectance = 0;
	}
	
	/**
	 * Initializes a material related only parameters
	 * 
	 * @param attributes
	 */
	public void initOnlyMaterialRelated(Map<String, String> attributes) {
		for (Map.Entry<String, String> item : attributes.entrySet()) {
			String key = item.getKey();
			String val = item.getValue();

			Scanner scanner = new Scanner(val);
			
			if (key.equalsIgnoreCase("mtl-diffuse")) {
				mtlDiffuse = new Vec(val);
			} else if (key.equalsIgnoreCase("mtl-specular")) {
				mtlSpecular = new Vec(val);
			} else if (key.equalsIgnoreCase("mtl-ambient")) {
				mtlAmbient = new Vec(val);
			} else if (key.equalsIgnoreCase("mtl-emission")) {
				mtlEmission = new Vec(val);
			} else if (key.equalsIgnoreCase("mtl-shininess")) {
				mtlShininess = scanner.nextDouble();
			} else if (key.equalsIgnoreCase("reflectance")) {
				reflectance = scanner.nextDouble();
			}  
			
			scanner.close();
		}
	}
	
	@Override
	public void init(Map<String, String> attributes) throws Exception {
		if (attributes == null) {
			throw new Exception("Surface attributes is null");
		}
		initOnlyMaterialRelated(attributes);
	}
	
	/**
	 * Get this surface emission
	 * 
	 * @return	Emission level
	 */
	public Vec getMtlEmission() {
		return mtlEmission;
	}
	
	/**
	 * Get this surface ambient
	 * 
	 * @return	Ambient level
	 */
	public Vec getMtlAmbient() {
		return mtlAmbient;
	}
	
	/**
	 * Get this surface diffuse
	 * 
	 * @return	Diffuse level
	 */
	public Vec getMtlDiffuse() {
		return mtlDiffuse;
	}
	
	/**
	 * Get this surface specular
	 * 
	 * @return	Specular level
	 */
	public Vec getMtlSpecular() {
		return mtlSpecular;
	}
	
	/**
	 * Get this surface shininess
	 * 
	 * @return	Shininess level
	 */
	public double getMtlShininess() {
		return mtlShininess;
	}
	
	/**
	 * Get this surface reflectance level
	 * 
	 * @return	Reflectance level
	 */
	public double getReflectance() {
		return reflectance;
	}

	/**
	 * Checks if a given ray 'r', intersects with this surface.
	 * This method is dependent on the surface type.
	 * 
	 * @param r			Some Ray
	 * @param facesUp	True if this surface should be facing up to the given ray,
	 * 					false otherwise.
	 * @return			if there was intersection Intersection object will be returned,
	 * 					otherwise null.
	 */
	public abstract Intersection intersects(Ray r, boolean facesUp);
	
	
	/**
	 * Get the normal to this surface at a given point 'p'
	 * 
	 * @param p 	A point on this surface  
	 * @return		Normal vector
	 */
	public abstract Vec getNormal(Point3D p);
}
