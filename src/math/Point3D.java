package math;
import java.util.Scanner;

/**
 * Point3D 
 * 
 * Describes a point in a 3D space
 */
public class Point3D {
	private double x;
	private double y;
	private double z;
	
	/**
	 * Initiate new point using scanner
	 * scanner assumed to be initiated with the correct XML content.
	 *  
	 * @param scanner
	 */
	public Point3D(Scanner scanner) {
		x = scanner.nextDouble();
		y = scanner.nextDouble();
		z = scanner.nextDouble();
	}

	/**
	 * Initiate new point from string
	 * @param v		String describing point (i.e.: -1 0 1) 
	 */
	public Point3D(String v) {
		Scanner s = new Scanner(v);
		x = s.nextDouble();
		y = s.nextDouble();
		z = s.nextDouble();
		s.close();
	}
	
	/**
	 * Initiates new Point3D using double values
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public Point3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Initiates new Point3D from an existing Point3D
	 * @param p
	 */
	public Point3D(Point3D p) {
		this.x = p.x;
		this.y = p.y;
		this.z = p.z;
	}
	
	/**
	 * Getter for point's X coordinate.
	 * @return Point's X coordinate
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Getter for point's Y coordinate.
	 * @return Point's Y coordinate
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Getter for point's Z coordinate.
	 * @return Point's Z coordinate
	 */
	public double getZ() {
		return z;
	}
	
	/**
	 * Add 'v' to this point
	 * 
	 * @param v 	Vector
	 */
	public void add(Vec v) {
		x += v.getX();
		y += v.getY();
		z += v.getZ();
	}
	
	/**
	 * Subtract 'v' from this point
	 * @param v
	 */
	public void sub(Vec v) {
		x -= v.getX();
		y -= v.getY();
		z -= v.getZ();
	}
	
	/**
	 * Return a direction vector from this vector to 'anotherPoint'
	 * 
	 * @param anotherPoint
	 * @return Direction vector
	 */
	public Vec directionTo(Point3D anotherPoint) {
		Vec v = new Vec(anotherPoint.x - x, anotherPoint.y - y, anotherPoint.z - z);
		v.normalize();
		return v;
	}
	
	/**
	 * Returns the distance of this point from 'anotherPoint'
	 * 
	 * @param anotherPoint
 	 * @return distance
	 */
	public double distanceFrom(Point3D anotherPoint) {
		return Math.sqrt(Math.pow(anotherPoint.x - x, 2) + Math.pow(anotherPoint.y - y, 2)
				 + Math.pow(anotherPoint.z - z,	2));
	}
	
	/**
	 * Check if 'p' == this point
	 * 
	 * @param p		point
	 * @return 		True if equal, false otherwise.
	 */
	public Boolean equals(Point3D p) {
		return x == p.x && y == p.y && z == p.z;
	}
	
	@Override
	public Point3D clone() {
		return new Point3D(this);
	}
}

