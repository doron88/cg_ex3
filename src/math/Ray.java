package math;

import ex3.render.raytrace.RayTracer;

/**
 * Ray class
 * 
 * Stores information about a shooted ray
 */
public class Ray {

	/***
	 * Point of origin
	 */
	private Point3D origin;
	/***
	 * Ray direction
	 */
	private Vec direction;
	
	/***
	 * Constructs a new ray
	 * @param origin - point of origin
	 * @param direction - ray direction
	 */
	public Ray(Point3D origin, Vec direction) {
		this.origin = origin;
		this.direction = direction;
		this.direction.normalize();
	}
	
	/***
	 * Getter for ray's origin
	 * @return ray's origin
	 */
	public Point3D getOrigin() {
		return origin;
	}
	
	/***
	 * Getter for ray's direction vector
	 * @return Ray's direction vector
	 */
	public Vec getDirection() {
		return direction;
	}
	
	/***
	 * Calculates the intersection point of the ray with a plane
	 * 
	 * @param pointWithinPlane
	 * @param planeNormal
	 * @return 	Intersection point
	 */
	public Point3D getIntersectionPoint(Point3D pointWithinPlane, Vec planeNormal) {
		Vec v1 = Vec.fromToPoints(origin, pointWithinPlane);
		double t = v1.dotProd(planeNormal) / direction.dotProd(planeNormal);
		if (t < RayTracer.EPSILON) {
			return null;
		}
		Point3D result = origin.clone();
		result.add(Vec.scale(t, direction));
		return result;
	}
	
	/**
	 * Get the reflected ray relative to 'normal'
	 * 
	 * @param point	
	 * @param normal
	 * @return
	 */
	public Ray getReflected(Point3D point, Vec normal) {
		return new Ray(point, direction.reflect(normal));
	}
}
