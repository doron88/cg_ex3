package math;

import java.awt.Color;
import java.util.Scanner;


/**
 * 3D vector class that contains three doubles. Could be used to represent
 * Vectors but also Points and Colors.
 * 
 */
public class Vec {

	/**
	 * Vector data. Allowed to be accessed publicly for performance reasons
	 */
	private double x;
	private double y;
	private double z;

	/**
	 * Initialize vector to (0,0,0)
	 */
	public Vec() {
		x = 0;
		y = 0;
		z = 0;
	}

	/**
	 * Initialize vector to given coordinates
	 * 
	 * @param x
	 *            Scalar
	 * @param y
	 *            Scalar
	 * @param z
	 *            Scalar
	 */
	public Vec(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec(Color color) {
		this.x = (double)color.getRed() / 256;
		this.y = (double)color.getGreen() / 256;
		this.z = (double)color.getBlue() / 256;
	}
	
	public Vec(String v) {
		Scanner s = new Scanner(v);
		this.x = s.nextDouble();
		this.y = s.nextDouble();
		this.z = s.nextDouble();
		s.close();
	}

	/**
	 * Initialize vector values to given vector (copy by value)
	 * 
	 * @param v
	 *            Vector
	 */
	public Vec(Vec v) {
		x = v.x;
		y = v.y;
		z = v.z;
	}
	
	/**
	 * Initiates a vector from a Point3D object
	 * @param p
	 */
	public Vec(Point3D p) {
		x = p.getX();
		y = p.getY();
		z = p.getZ();
	}
	
	/**
	 * Getter for vector's X coordinate.
	 * @return Vector's X coordinate
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Getter for vector's Y coordinate.
	 * @return Vector's Y coordinate
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Getter for vector's Z coordinate.
	 * @return Vector's Z coordinate
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Calculates the reflection of the vector in relation to a given surface
	 * normal. The vector points at the surface and the result points away.
	 *
	 * @return The reflected vector
	 */
	public Vec reflect(Vec normal) {
		Vec v = new Vec(this);
		double scalar = (normal.dotProd(this)) / normal.length();
		Vec normalizedN = new Vec(normal);
		normalizedN.normalize();
		normalizedN.scale(scalar * 2);
		v.sub(normalizedN);
		
		return v;
	}

	/**
	 * Adds a to vector
	 * 
	 * @param a
	 *            Vector
	 */
	public void add(Vec a) {
		x += a.x;
		y += a.y;
		z += a.z;
	}

	/**
	 * Subtracts from vector
	 * 
	 * @param a
	 *            Vector
	 */
	public void sub(Vec a) {
		x -= a.x;
		y -= a.y;
		z -= a.z;
	}
	
	/**
	 * Multiplies & Accumulates vector with given vector and a. v := v + s*a
	 * 
	 * @param s
	 *            Scalar
	 * @param a
	 *            Vector
	 */
	public void mac(double s, Vec a) {
		x += s * a.x;
		y += s * a.y;
		z += s * a.z;
	}

	/**
	 * Multiplies vector with scalar. v := s*v
	 * 
	 * @param s
	 *            Scalar
	 */
	public void scale(double s) {
		x *= s;
		y *= s;
		z *= s;
	}

	/**
	 * Pairwise multiplies with another vector
	 * 
	 * @param a
	 *            Vector
	 */
	public void scale(Vec a) {
		x *= a.x;
		y *= a.y;
		z *= a.z;
	}

	/**
	 * Inverses vector
	 * 
	 * @return Vector
	 */
	public void negate() {
		x *= -1;
		y *= -1;
		z *= -1;
	}

	/**
	 * Computes the vector's magnitude
	 * 
	 * @return Scalar
	 */
	public double length() {
		return Math.sqrt(x * x + y * y + z * z);
	}

	/**
	 * Computes the vector's magnitude squared. Used for performance gain.
	 * 
	 * @return Scalar
	 */
	public double lengthSquared() {
		return x * x + y * y + z * z;
	}

	/**
	 * Computes the dot product between two vectors
	 * 
	 * @param a
	 *            Vector
	 * @return Scalar
	 */
	public double dotProd(Vec a) {
		return x * a.x + y * a.y + z * a.z;
	}

	/**
	 * Normalizes the vector to have length 1. Throws exception if magnitude is zero.
	 * 
	 * @throws ArithmeticException
	 */
	public void normalize() throws ArithmeticException {
		double m = length();
		if (0 == m) {
			throw new ArithmeticException();
		}
		
		x /= m;
		y /= m;
		z /= m;
	}

	/**
	 * Compares to a given vector
	 * 
	 * @param a
	 *            Vector
	 * @return True if have same values, false otherwise
	 */
	public boolean equals(Vec a) {
		return ((a.x == x) && (a.y == y) && (a.z == z));
	}

	/**
	 * Returns the angle in radians between this vector and the vector
	 * parameter; the return value is constrained to the range [0,PI].
	 * 
	 * @param v1
	 *            the other vector
	 * @return the angle in radians in the range [0,PI]
	 */
	public final double angle(Vec v1) {
		return Math.acos(dotProd(v1) / (length() * v1.length()));
	}

	/**
	 * Computes the Euclidean distance between two points
	 * 
	 * @param a
	 *            Point1
	 * @param b
	 *            Point2
	 * @return Scalar
	 */
	static public double distance(Vec a, Vec b) {
		return Math.sqrt(Math.pow((a.x - b.x), 2) + 
				Math.pow((a.y - b.y), 2) + 
				Math.pow((a.z - b.z), 2)); 
	}

	/**
	 * Computes the cross product between two vectors using the right hand rule
	 * 
	 * @param a
	 *            Vector1
	 * @param b
	 *            Vector2
	 * @return Vector1 x Vector2
	 */
	public static Vec crossProd(Vec a, Vec b) {	
		Vec v = new Vec();
		v.x = a.y * b.z - a.z * b.y;
		v.y = a.z * b.x - a.x * b.z;
		v.z = a.x * b.y - a.y * b.x;
		
		return v;
	}
	
	/**
	 * Adds vectors a and b
	 * 
	 * @param a
	 *            Vector
	 * @param b
	 *            Vector
	 * @return a+b
	 */
	public static Vec add(Vec a, Vec b) {
		Vec c = new Vec(a);
		c.add(b);
		
		return c;
	}

	/**
	 * Subtracts vector b from a
	 * 
	 * @param a
	 *            Vector
	 * @param b
	 *            Vector
	 * @return a-b
	 */
	public static Vec sub(Vec a, Vec b) {
		Vec c = new Vec(a);
		c.sub(b);
		
		return c;
	}

	/**
	 * Inverses vector's direction
	 * 
	 * @param a
	 *            Vector
	 * @return -1*a
	 */
	public static Vec negate(Vec a) {
		Vec c = new Vec(a);
		c.negate();
		
		return c;
	}

	/**
	 * Scales vector a by scalar s
	 * 
	 * @param s
	 *            Scalar
	 * @param a
	 *            Vector
	 * @return s*a
	 */
	public static Vec scale(double s, Vec a) {
		Vec c = new Vec(a);
		c.scale(s);
		
		return c;
	}

	/**
	 * Pair-wise scales vector a by vector b
	 * 
	 * @param a
	 *            Vector
	 * @param b
	 *            Vector
	 * @return a.*b
	 */
	public static Vec scale(Vec a, Vec b) {
		Vec c = new Vec(a);
		c.scale(b);
		
		return c;
	}

	/**
	 * Compares vector a to vector b
	 * 
	 * @param a
	 *            Vector
	 * @param b
	 *            Vector
	 * @return a==b
	 */
	public static boolean equals(Vec a, Vec b) {
		return a.equals(b);
	}

	/**
	 * Dot product of a and b
	 * 
	 * @param a
	 *            Vector
	 * @param b
	 *            Vector
	 * @return a.b
	 */
	public static double dotProd(Vec a, Vec b) {
		return a.dotProd(b);
	}
	
	/**
	 * Calculates a vector directed from point 'a' to point 'b'
	 * 
	 * @param a		From point
	 * @param b		To point
	 * @return		Resulting vector
	 */
	public static Vec fromToPoints(Point3D a, Point3D b) {
		return new Vec(b.getX() - a.getX(), b.getY() - a.getY(), b.getZ() - a.getZ());
	}

	/**
	 * Returns a string that contains the values of this vector. The form is
	 * (x,y,z).
	 * 
	 * @return the String representation
	 */
	public String toString() {
		return "(" + this.x + ", " + this.y + ", " + this.z + ")";
	}
	
	/**
	 * Conver's the vector in a color object
	 * @return Color object
	 */
	public Color toColor() {
		double r = x;
		double g = y;
		double b = z;
		
		if (r > 1) {
			r = 1;
		}
		if (r < 0) {
			r = 0;
		}
		if (g > 1) {
			g = 1;
		}
		if (g < 0) {
			g = 0;
		}
		if (b > 1) {
			b = 1;
		}
		if (b < 0) {
			b = 0;
		}
		
		return new Color((float)r, (float)g, (float)b);
	}
	
	@Override
	public Vec clone() {
		return new Vec(this);
	}
}
